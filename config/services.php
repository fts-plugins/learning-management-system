<?php

use FantassinCoreWordPressVendor\Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return function (ContainerConfigurator $configurator) {
    $services = $configurator->services()
                             ->defaults()
                             ->bind('$pluginDirectory', '%pluginDirectory%')
                             ->autowire()       // Automatically injects dependencies in your services.
                             ->autoconfigure(); // Automatically registers your services as commands, event subscribers, etc.

    // This creates a service per class whose id is the fully-qualified class name.
    $services->load('Fantassin\\LearningManagementSystem\\', '../includes/*')
             ->exclude('../includes/{Entity,Tests}');
};

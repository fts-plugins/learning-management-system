<?php
/**
 * Plugin Name: Fantassin — Learning Management System
 */

use Fantassin\Core\WordPress\Plugin\PluginKernel;
use Fantassin\LearningManagementSystem\Constants;

require 'vendor/autoload.php';

class FantassinLearningManagementSystem extends PluginKernel
{

    public function getTextDomain(): string
    {
        return Constants::TEXT_DOMAIN;
    }
}

$fantassinLMS = new FantassinLearningManagementSystem(wp_get_environment_type(), WP_DEBUG);

add_action('plugins_loaded', [$fantassinLMS, 'load']);

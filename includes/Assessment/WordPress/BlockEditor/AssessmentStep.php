<?php

namespace Fantassin\LearningManagementSystem\Assessment\WordPress\BlockEditor;

use Fantassin\Core\WordPress\Blocks\CustomBlock;

class AssessmentStep extends CustomBlock
{
    protected string $name = 'assessment-step';
}

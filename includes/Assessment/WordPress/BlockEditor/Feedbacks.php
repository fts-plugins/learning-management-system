<?php

namespace Fantassin\LearningManagementSystem\Assessment\WordPress\BlockEditor;

use Fantassin\Core\WordPress\Blocks\CustomBlock;
use Fantassin\Core\WordPress\Contracts\DynamicBlock;

class Feedbacks extends CustomBlock implements DynamicBlock
{
    protected string $name = 'feedbacks';

    public function renderBlock(array $attributes, string $content): string
    {
        // var_dump($attributes);

        return $content;
    }
}

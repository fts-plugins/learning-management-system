<?php

namespace Fantassin\LearningManagementSystem\Assessment\WordPress\BlockEditor;

use Fantassin\Core\WordPress\Blocks\CustomBlock;
use Fantassin\Core\WordPress\Contracts\DynamicBlock;

class Label extends CustomBlock
{
    protected string $name = 'label';
}

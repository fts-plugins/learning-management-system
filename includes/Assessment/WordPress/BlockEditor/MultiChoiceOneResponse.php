<?php

namespace Fantassin\LearningManagementSystem\Assessment\WordPress\BlockEditor;

use Fantassin\Core\WordPress\Blocks\CustomBlock;
use Fantassin\Core\WordPress\Contracts\DynamicBlock;

class MultiChoiceOneResponse extends CustomBlock
{
    protected string $name = 'multi-choice-one-response';
}

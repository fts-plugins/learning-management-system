<?php

namespace Fantassin\LearningManagementSystem\Assessment\WordPress\BlockEditor;

use Fantassin\Core\WordPress\Blocks\CustomBlock;
use Fantassin\Core\WordPress\Contracts\DynamicBlock;

class Question extends CustomBlock implements DynamicBlock
{
    protected string $name = 'question';

    public function renderBlock(array $attributes, string $content): string
    {
        return $content;
    }
}

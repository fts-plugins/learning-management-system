<?php

namespace Fantassin\LearningManagementSystem\Assessment\WordPress\PostType;

use Fantassin\Core\WordPress\PostType\Contracts\PostTypeInterface;
use Fantassin\LearningManagementSystem\Constants;

class AssessmentPostType implements PostTypeInterface
{

    public function getKey(): ?string
    {
        return Constants::ASSESSMENT_POST_TYPE;
    }

    public function getArgs(): array
    {
        return [
            'labels' => [
                'name' => __( 'Assessments', Constants::TEXT_DOMAIN ),
                'singular_name' => __( 'Assessment', Constants::TEXT_DOMAIN )
           ]
        ];
    }
}

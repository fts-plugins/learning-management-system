<?php

namespace Fantassin\LearningManagementSystem\Assessment\WordPress\View;

use Fantassin\Core\WordPress\Contracts\Hooks;
use Fantassin\LearningManagementSystem\Constants;
use Fantassin\LearningManagementSystem\WordPress\Repository\WorkshopRepository;

class AssessmentStepForm implements Hooks
{

    const CLASSNAME = 'class="wp-block-fantassin-assessment-step"';

    public function hooks()
    {
        add_filter('the_content', [$this, 'displayRegistrationForm'], 10, 2);
    }

    public function displayRegistrationForm(string $content): string
    {
        if (get_post_type(get_the_ID()) !== Constants::ASSESSMENT_POST_TYPE) {
            return $content;
        }

        return $this->identifyStep($content);
    }

    /**
     * @param string $content
     *
     * @return string
     */
    public function identifyStep(string $content): string
    {
        return preg_replace_callback(
            '/<div ' . self::CLASSNAME . '>.*?<\/div>/s',
            [$this, 'convertToForm'],
            $content
        );
    }

    public function convertToForm(array $matches)
    {
        $match = current($matches);

        $content = str_replace('<div ' . self::CLASSNAME, '<form ' . self::CLASSNAME, $match);
        // Delete last </div> characters.
        $content = substr($content, 0, -6);
        $content .= '</form>';

        return $content;
    }
}

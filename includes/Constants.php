<?php

namespace Fantassin\LearningManagementSystem;

class Constants
{

    const COURSE_POST_TYPE = 'fantassin_course';
    const ASSESSMENT_POST_TYPE = 'fantassin_assessment';
    const WORKSHOP_POST_TYPE = 'fantassin_workshop';

    const WORKSHOP_POST_META = 'fantassin_workshop_post_meta';
    const COURSE_INDEX = 'course';
    const PARTICIPANTS_INDEX = 'participants';
    const REGISTRATION_IS_OPEN_INDEX = 'registration_is_open';
    const FIRSTNAME_INDEX = 'firstname';
    const LASTNAME_INDEX = 'lastname';
    const WORKSHOP_ID_INDEX = 'workshop_id';
    const EMAIL_INDEX = 'email';

    const REGISTER_FOR_WORKSHOP_ACTION = 'fantassin_register_for_workshop_action';
    const REGISTER_FOR_WORKSHOP_NONCE = 'fantassin_register_for_workshop_nonce';

    const TEXT_DOMAIN = 'fantassin-learning-management-system';

}

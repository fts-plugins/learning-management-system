<?php

namespace Fantassin\LearningManagementSystem\Entity;

class Course
{
    var int $id;
    var string $title = '';
    var array $program = [];

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Course
     */
    public function setTitle(string $title): Course
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return array
     */
    public function getProgram(): array
    {
        return $this->program;
    }

    /**
     * @param array $program
     *
     * @return Course
     */
    public function setProgram(array $program): Course
    {
        $this->program = $program;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Course
     */
    public function setId(int $id): Course
    {
        $this->id = $id;

        return $this;
    }
}

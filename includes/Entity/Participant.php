<?php

namespace Fantassin\LearningManagementSystem\Entity;

class Participant
{
    var int $id;
    var string $firstname = '';
    var string $lastname = '';
    var string $email = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Participant
     */
    public function setId(int $id): Participant
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     *
     * @return Participant
     */
    public function setFirstname(string $firstname): Participant
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     *
     * @return Participant
     */
    public function setLastname(string $lastname): Participant
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return Participant
     */
    public function setEmail(string $email): Participant
    {
        $this->email = $email;

        return $this;
    }
}

<?php

namespace Fantassin\LearningManagementSystem\Entity;

use DateTimeInterface;

class Session
{
    var DateTimeInterface $date;
    var string $location = '';

    /**
     * @return DateTimeInterface
     */
    public function getDate(): DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @param DateTimeInterface $date
     *
     * @return Session
     */
    public function setDate(DateTimeInterface $date): Session
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $location
     *
     * @return Session
     */
    public function setLocation(string $location): Session
    {
        $this->location = $location;

        return $this;
    }
}

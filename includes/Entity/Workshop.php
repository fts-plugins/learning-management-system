<?php

namespace Fantassin\LearningManagementSystem\Entity;

use Fantassin\LearningManagementSystem\WordPress\Repository\WorkshopRepository;

class Workshop
{
    var int $id;
    var string $title = '';
    var ?Course $course = null;
    var bool $registrationIsOpen = false;

    /**
     * @var Participant[]
     */
    var array $participants = [];

    /**
     * @var Session[]
     */
    var array $sessions = [];

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Workshop
     */
    public function setTitle(string $title): Workshop
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Course|null
     */
    public function getCourse(): ?Course
    {
        return $this->course;
    }

    /**
     * @param Course $course
     *
     * @return Workshop
     */
    public function setCourse(Course $course): Workshop
    {
        $this->course = $course;

        return $this;
    }

    /**
     * @return Participant[]
     */
    public function getParticipants(): array
    {
        return $this->participants;
    }

    /**
     * @param Participant $participant
     *
     * @return Workshop
     */
    public function addParticipant(Participant $participant): Workshop
    {
        $this->participants[] = $participant;

        return $this;
    }

    /**
     * @param Participant[] $participants
     *
     * @return Workshop
     */
    public function setParticipants(array $participants): Workshop
    {
        $this->participants = $participants;

        return $this;
    }

    /**
     * @return Session[]
     */
    public function getSessions(): array
    {
        return $this->sessions;
    }

    /**
     * @param Session $session
     *
     * @return Workshop
     */
    public function addSession(Session $session): Workshop
    {
        $this->sessions[] = $session;

        return $this;
    }

    /**
     * @param Session[] $sessions
     */
    public function setSessions(array $sessions): Workshop
    {
        $this->sessions = $sessions;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Workshop
     */
    public function setId(int $id): Workshop
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return bool
     */
    public function getRegistrationIsOpen(): bool
    {
        return $this->registrationIsOpen;
    }

    /**
     * @param bool $registrationIsOpen
     *
     * @return Workshop
     */
    public function setRegistrationIsOpen(bool $registrationIsOpen): Workshop
    {
        $this->registrationIsOpen = $registrationIsOpen;

        return $this;
    }

}

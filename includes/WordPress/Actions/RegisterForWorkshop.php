<?php

namespace Fantassin\LearningManagementSystem\WordPress\Actions;

use Fantassin\Core\WordPress\Contracts\Hooks;
use Fantassin\LearningManagementSystem\Constants;
use Fantassin\LearningManagementSystem\WordPress\Repository\ParticipantRepository;
use Fantassin\LearningManagementSystem\WordPress\Repository\WorkshopRepository;

class RegisterForWorkshop implements Hooks
{

    protected WorkshopRepository $repository;
    protected ParticipantRepository $participantRepository;

    public function __construct(
        WorkshopRepository $repository,
        ParticipantRepository $participantRepository
    )
    {
        $this->repository = $repository;
        $this->participantRepository = $participantRepository;
    }

    public function hooks()
    {
        add_action('admin_post_' . Constants::REGISTER_FOR_WORKSHOP_ACTION, [$this, 'registerForWorkshop']);
        add_action('admin_post_nopriv_' . Constants::REGISTER_FOR_WORKSHOP_ACTION, [$this, 'registerForWorkshop']);
    }

    public function registerForWorkshop()
    {
        if (
            ! array_key_exists(Constants::REGISTER_FOR_WORKSHOP_NONCE, $_POST)
            || wp_verify_nonce($_POST[Constants::REGISTER_FOR_WORKSHOP_NONCE], Constants::REGISTER_FOR_WORKSHOP_ACTION)
        ) {
            $errors['csrf'] = 'Votre token a expiré, veuillez réactualiser votre page';
        }

        $firstname = null;
        $lastname  = null;
        $email     = null;
        $errors    = [];

        if (
            ! array_key_exists(Constants::FIRSTNAME_INDEX, $_POST)
            || empty($_POST[Constants::FIRSTNAME_INDEX])
        ) {
            $errors[Constants::FIRSTNAME_INDEX] = 'Veuillez renseigner un prénom';
        }

        if (
            ! array_key_exists(Constants::LASTNAME_INDEX, $_POST)
            || empty($_POST[Constants::LASTNAME_INDEX])
        ) {
            $errors[Constants::LASTNAME_INDEX] = 'Veuillez renseigner un nom de famille';
        }

        if (
            ! array_key_exists(Constants::EMAIL_INDEX, $_POST)
            || empty($_POST[Constants::EMAIL_INDEX])
            || ! is_email($_POST[Constants::EMAIL_INDEX])
        ) {
            $errors[Constants::EMAIL_INDEX] = "Veuillez renseigner un e-mail valide pour que l'on puisse vous contacter";
        }

        if (
            ! array_key_exists(Constants::WORKSHOP_ID_INDEX, $_POST)
            || empty($_POST[Constants::WORKSHOP_ID_INDEX])
        ) {
            $errors[Constants::WORKSHOP_ID_INDEX] = "Petit filou !";
        }

        if (count($errors) > 0) {
            $title   = "Veuillez corriger les erreurs suivantes s'il vous plait";
            $message = '<h1>' . $title . '</h1>';
            $message .= '<ul>';
            foreach ($errors as $error) {
                $message .= '<li>' . $error . '</li>';
            }
            $message .= '</ul>';

            $args = [
                'back_link' => true
            ];
            wp_die($message, $title, $args);
        }

        $firstname  = sanitize_text_field($_POST[Constants::FIRSTNAME_INDEX]);
        $lastname   = sanitize_text_field($_POST[Constants::LASTNAME_INDEX]);
        $email      = sanitize_email($_POST[Constants::EMAIL_INDEX]);
        $workshopId = intval($_POST[Constants::WORKSHOP_ID_INDEX]);

        $userId = register_new_user( $email, $email );
        update_user_meta($userId, 'display_name', $firstname . ' ' . $lastname );
        update_user_meta($userId, 'first_name', $firstname);
        update_user_meta($userId, 'last_name', $lastname);

        if ( is_wp_error($userId)){
            $user = get_user_by_email($email);
            $userId = $user->ID;
        }

        $metas = get_post_meta($workshopId, Constants::WORKSHOP_POST_META, true);

        // Create meta array if not exists.
        if( ! is_array($metas) ){
            $metas = [];
        }

        // Create users array if not exists.
        if( ! array_key_exists(Constants::PARTICIPANTS_INDEX, $metas)){
            $metas[Constants::PARTICIPANTS_INDEX] = [];
        }

        // Prevent doublons.
        if( ! is_wp_error($userId) && ! in_array($userId, $metas[Constants::PARTICIPANTS_INDEX])) {
            $metas[Constants::PARTICIPANTS_INDEX][] = $userId;
            // Send welcome e-mail.
        }

        update_post_meta($workshopId, Constants::WORKSHOP_POST_META, $metas);

        wp_safe_redirect(wp_get_referer());
        exit;
    }
}

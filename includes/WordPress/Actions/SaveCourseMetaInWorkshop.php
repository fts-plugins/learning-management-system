<?php

namespace Fantassin\LearningManagementSystem\WordPress\Actions;

use Fantassin\Core\WordPress\Contracts\AdminHooks;
use Fantassin\LearningManagementSystem\Constants;
use Fantassin\LearningManagementSystem\Entity\Course;
use Fantassin\LearningManagementSystem\WordPress\Repository\CourseRepository;
use WP_Post;

class

SaveCourseMetaInWorkshop implements AdminHooks
{

    public function hooks()
    {
        add_action('save_post', [$this, 'saveCourseMeta'], 10, 2);
    }

    public function saveCourseMeta(int $postId, WP_Post $post)
    {
        // Only set for post_type = post!
        if ($post->post_type !== Constants::WORKSHOP_POST_TYPE) {
            return;
        }

        if (
            ! array_key_exists(Constants::WORKSHOP_POST_META, $_POST)
            || ! is_array($_POST[Constants::WORKSHOP_POST_META])
            || ! array_key_exists(Constants::COURSE_INDEX, $_POST[Constants::WORKSHOP_POST_META])
        ) {
            return;
        }

        $meta = get_post_meta($postId, Constants::WORKSHOP_POST_META, true);

        if ( ! is_array($meta)) {
            $meta = [];
        }

        $courseId  = intval($_POST[Constants::WORKSHOP_POST_META][Constants::COURSE_INDEX]);
        $getCourse = get_post($courseId);

        if (is_null($getCourse)) {
            return;
        }

        $meta[Constants::COURSE_INDEX] = $courseId;

        update_post_meta($postId, Constants::WORKSHOP_POST_META, $meta);
    }
}

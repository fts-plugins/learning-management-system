<?php

namespace Fantassin\LearningManagementSystem\WordPress\Admin;

use Fantassin\Core\WordPress\Contracts\AdminHooks;
use Fantassin\LearningManagementSystem\Constants;

class DisableBlockEditorForBootcamp implements AdminHooks
{

    public function hooks()
    {
        add_filter('use_block_editor_for_post_type', [$this, 'disableBlockEditor'], 10, 2);
    }

    public function disableBlockEditor($current_status, $post_type): bool
    {
        // Use your post type key instead of 'product'
        if ($post_type === Constants::WORKSHOP_POST_TYPE) {
            return false;
        }

        return $current_status;
    }
}

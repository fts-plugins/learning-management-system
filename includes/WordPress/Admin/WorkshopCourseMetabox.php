<?php

namespace Fantassin\LearningManagementSystem\WordPress\Admin;

use Fantassin\Core\WordPress\Contracts\AdminHooks;
use Fantassin\LearningManagementSystem\Constants;
use Fantassin\LearningManagementSystem\WordPress\Repository\WorkshopRepository;
use Fantassin\LearningManagementSystem\WordPress\Repository\CourseRepository;
use WP_Post;

class WorkshopCourseMetabox implements AdminHooks
{

    protected CourseRepository $courseRepository;
    protected WorkshopRepository $workshopRepository;

    public function __construct(CourseRepository $courseRepository, WorkshopRepository $bootcampRepository){
        $this->courseRepository   = $courseRepository;
        $this->workshopRepository = $bootcampRepository;
    }

    public function hooks()
    {
        add_action('add_meta_boxes', [$this, 'registerMetabox']);
    }

    public function registerMetabox()
    {
        add_meta_box(
            'fantassin-bootcamp-course',
            esc_html__('Course', 'fantassin-learning-management-system'),
            [$this, 'renderMetabox'],
            Constants::WORKSHOP_POST_TYPE,
            'normal',
            'high'
        );
    }

    public function renderMetabox( WP_Post $post )
    {
        $courses = $this->courseRepository->findAll();
        $workshop = $this->workshopRepository->findById($post->ID);
        $currentCourseId = null;

        if( ! is_null( $workshop->getCourse() ) ) {
            $currentCourseId = $workshop->getCourse()->getId();
        }

        ?>
            <div>
                <label for="workshop-course"><?php _e('Course', Constants::TEXT_DOMAIN); ?></label>
                <select name="<?php esc_attr_e(Constants::WORKSHOP_POST_META . '[' . Constants::COURSE_INDEX . ']' ); ?>" id="workshop-course">
                    <option value="-1">None</option>
                    <?php foreach( $courses as $course ) : ?>
                        <option
                                value="<?php esc_attr_e( $course->getId() ); ?>"
                                <?php selected( $currentCourseId, $course->getId() ); ?>
                        >
                            <?php esc_html_e( $course->getTitle() ); ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div>
                <label for="workshop-registration-is-open"><?php _e('Registration is open ?', Constants::TEXT_DOMAIN); ?></label>
                <input
                        <?php checked($workshop->getRegistrationIsOpen()) ?>
                        type="checkbox"
                        name="<?php esc_attr_e(Constants::WORKSHOP_POST_META . '[' . Constants::REGISTRATION_IS_OPEN_INDEX . ']' ); ?>"
                        id="workshop-registration-is-open"
                >
            </div>

            <?php if (count($workshop->getParticipants()) > 0) : ?>
                <div>
                    <ul>
                        <?php foreach( $workshop->getParticipants() as $participant ) : ?>
                            <li><?php echo $participant->getFirstname() . ' ' . $participant->getLastname() . ' — ' . $participant->getEmail(); ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        <?php
    }
}

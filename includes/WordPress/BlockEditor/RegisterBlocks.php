<?php

namespace Fantassin\LearningManagementSystem\WordPress\BlockEditor;

use Fantassin\Core\WordPress\Blocks\BlockRegistry;
use Fantassin\Core\WordPress\Contracts\DynamicBlock;
use Fantassin\Core\WordPress\Contracts\Hooks;

class RegisterBlocks implements Hooks
{
    protected string $pluginDirectory;
    protected BlockRegistry $registry;

    public function __construct(string $pluginDirectory, BlockRegistry $registry)
    {
        $this->pluginDirectory = $pluginDirectory;
        $this->registry        = $registry;
    }

    public function hooks()
    {
        add_action('init', [$this, 'registerBlocksFromBuildDirectory']);
    }

    public function registerBlocksFromBuildDirectory()
    {
        if ( ! file_exists($this->pluginDirectory . '/build')) {
            return;
        }

        foreach ($this->registry->getBlocks() as $block) {
            $args = [
                'api_version'   => 2,
            ];

            if ($block instanceof DynamicBlock) {
                $args['render_callback'] = [$block, 'renderBlock'];
            }
            
            register_block_type($this->pluginDirectory . '/build/blocks/' . $block->getName(), $args);
        }
    }
}

<?php

namespace Fantassin\LearningManagementSystem\WordPress\Factory;

use Fantassin\LearningManagementSystem\Entity\Course;
use WP_Post;

class CourseFactory
{

    /**
     * @param WP_Post[] $posts
     *
     * @return Course[]
     */
    public function createFromPosts(array $posts): array
    {
        return array_map([$this, 'createFromPost'], $posts);
    }

    /**
     * @param WP_Post $post
     *
     * @return Course
     */
    public function createFromPost(WP_Post $post): Course
    {
        return (new Course())
            ->setId($post->ID)
            ->setTitle($post->post_title);
    }

}

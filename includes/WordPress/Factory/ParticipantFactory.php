<?php

namespace Fantassin\LearningManagementSystem\WordPress\Factory;

use Fantassin\LearningManagementSystem\Entity\Course;
use Fantassin\LearningManagementSystem\Entity\Participant;
use WP_Post;
use WP_User;

class ParticipantFactory
{

    /**
     * @param WP_User[] $users
     *
     * @return Course[]
     */
    public function createFromUsers(array $users): array
    {
        return array_map([$this, 'createFromPost'], $users);
    }

    /**
     * @param WP_User $user
     *
     * @return Participant
     */
    public function createFromUser(WP_User $user): Participant
    {
        return (new Participant())
            ->setId($user->ID)
            ->setEmail($user->user_email)
            ->setLastname($user->user_lastname)
            ->setFirstname($user->user_firstname);
    }

}

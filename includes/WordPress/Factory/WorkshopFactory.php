<?php

namespace Fantassin\LearningManagementSystem\WordPress\Factory;

use Fantassin\LearningManagementSystem\Entity\Workshop;
use WP_Post;

class WorkshopFactory
{

    public function createFromPost(WP_Post $post): Workshop
    {
        return (new Workshop())
            ->setId($post->ID)
            ->setTitle($post->post_title);
    }

    /**
     * @param WP_Post[] $posts
     *
     * @return Workshop
     */
    public function createFromPosts(array $posts): Workshop
    {
        return array_map([$this, 'createFromPost'], $posts);
    }

}

<?php

namespace Fantassin\LearningManagementSystem\WordPress\PostType;

use Fantassin\Core\WordPress\PostType\Contracts\PostTypeInterface;
use Fantassin\LearningManagementSystem\Constants;

class CoursePostType implements PostTypeInterface
{

    public function getKey(): ?string
    {
        return Constants::COURSE_POST_TYPE;
    }

    public function getArgs(): array
    {
        return [
            'labels' => [
                'name' => __( 'Courses', Constants::TEXT_DOMAIN ),
                'singular_name' => __( 'Course', Constants::TEXT_DOMAIN )
           ]
        ];
    }
}

<?php

namespace Fantassin\LearningManagementSystem\WordPress\PostType;

use Fantassin\Core\WordPress\PostType\Contracts\PostTypeInterface;
use Fantassin\LearningManagementSystem\Constants;

class WorkshopPostType implements PostTypeInterface
{

    public function getKey(): ?string
    {
        return Constants::WORKSHOP_POST_TYPE;
    }

    public function getArgs(): array
    {
        return [
            'labels'   => [
                'name'          => __('Workshops', Constants::TEXT_DOMAIN),
                'singular_name' => __('Workshop', Constants::TEXT_DOMAIN)
            ],
            'supports' => ['title'],
            'public' => true
        ];
    }
}

<?php

namespace Fantassin\LearningManagementSystem\WordPress\Repository;

use Fantassin\LearningManagementSystem\Constants;
use Fantassin\LearningManagementSystem\Entity\Course;
use Fantassin\LearningManagementSystem\WordPress\Factory\CourseFactory;

class CourseRepository
{
    protected CourseFactory $factory;

    public function __construct(CourseFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @return Course[]
     */
    function findAll(): array
    {
        $args  = [
            'post_type'      => Constants::COURSE_POST_TYPE,
            'posts_per_page' => -1
        ];
        $posts = get_posts($args);

        return $this->factory->createFromPosts($posts);
    }

    public function findById(int $id): Course
    {
        $post = get_post($id);

        return $this->factory->createFromPost($post);
    }

}

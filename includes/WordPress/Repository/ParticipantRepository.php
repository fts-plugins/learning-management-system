<?php

namespace Fantassin\LearningManagementSystem\WordPress\Repository;

use Exception;
use Fantassin\LearningManagementSystem\Constants;
use Fantassin\LearningManagementSystem\Entity\Participant;
use Fantassin\LearningManagementSystem\Entity\Workshop;
use Fantassin\LearningManagementSystem\Entity\Course;
use Fantassin\LearningManagementSystem\WordPress\Factory\ParticipantFactory;
use Fantassin\LearningManagementSystem\WordPress\Factory\WorkshopFactory;
use Fantassin\LearningManagementSystem\WordPress\Factory\CourseFactory;

class ParticipantRepository
{
    protected ParticipantFactory $factory;

    public function __construct(ParticipantFactory $factory)
    {
        $this->factory = $factory;
    }

    public function findById(int $id): Participant
    {
        $user = get_user_by('id', $id);

        if( ! $user ){
            throw new Exception(sprintf( 'No user found with id %s', $id));
        }

        return $this->factory->createFromUser($user);
    }

    /**
     * @return Participant[]
     * @throws Exception
     */
    public function findAllByWorkshopId(int $id): array
    {
        $metas = get_post_meta($id, Constants::WORKSHOP_POST_META, true);

        // Create meta array if not exists.
        if ( ! is_array($metas)) {
            $metas = [];
        }

        // Create participant array if not exists.
        if ( ! array_key_exists(Constants::PARTICIPANTS_INDEX, $metas)) {
            $metas[Constants::PARTICIPANTS_INDEX] = [];
        }

        return array_map(function ($userId) {
            return $this->findById($userId);
        }, $metas[Constants::PARTICIPANTS_INDEX]);
    }

}

<?php

namespace Fantassin\LearningManagementSystem\WordPress\Repository;

use Fantassin\LearningManagementSystem\Constants;
use Fantassin\LearningManagementSystem\Entity\Workshop;
use Fantassin\LearningManagementSystem\Entity\Course;
use Fantassin\LearningManagementSystem\WordPress\Factory\WorkshopFactory;
use Fantassin\LearningManagementSystem\WordPress\Factory\CourseFactory;

class WorkshopRepository
{
    protected WorkshopFactory $factory;
    protected CourseRepository $courseRepository;
    protected ParticipantRepository $participantRepository;

    public function __construct(
        WorkshopFactory $factory,
        CourseRepository $courseRepository,
        ParticipantRepository $participantRepository
    )
    {
        $this->factory = $factory;
        $this->courseRepository = $courseRepository;
        $this->participantRepository = $participantRepository;
    }

    public function findById(int $id): Workshop
    {
        $post = get_post($id);
        $workshop = $this->factory->createFromPost($post);

        $metas = get_post_meta($post->ID, Constants::WORKSHOP_POST_META, true);

        if(
            is_array($metas)
            && array_key_exists(Constants::COURSE_INDEX, $metas)
            && $metas[Constants::COURSE_INDEX] !== -1
        ){
            $course = $this->courseRepository->findById($metas[Constants::COURSE_INDEX]);
            $workshop->setCourse($course);
        }

        if(
            is_array($metas)
            && array_key_exists(Constants::REGISTRATION_IS_OPEN_INDEX, $metas)
        ){
            $workshop->setRegistrationIsOpen($metas[Constants::REGISTRATION_IS_OPEN_INDEX]);
        }

        if(
            is_array($metas)
            && array_key_exists(Constants::PARTICIPANTS_INDEX, $metas)
        ){
            foreach( $metas[Constants::PARTICIPANTS_INDEX] as $userId ){
                $participant = $this->participantRepository->findById($userId);
                $workshop->addParticipant($participant);
            }
        }


        return $workshop;
    }

    /**
     * @return Workshop[]
     */
    public function findAll(): array
    {
        $args  = [
            'post_type'      => Constants::WORKSHOP_POST_TYPE,
            'posts_per_page' => -1
        ];
        $posts = get_posts($args);

        return $this->factory->createFromPosts($posts);
    }

}

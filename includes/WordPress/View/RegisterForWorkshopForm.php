<?php

namespace Fantassin\LearningManagementSystem\WordPress\View;

use Fantassin\Core\WordPress\Contracts\Hooks;
use Fantassin\LearningManagementSystem\Constants;
use Fantassin\LearningManagementSystem\WordPress\Repository\WorkshopRepository;

class RegisterForWorkshopForm implements Hooks
{

    protected WorkshopRepository $repository;

    public function __construct( WorkshopRepository $repository){
        $this->repository = $repository;
    }

    public function hooks()
    {
        add_filter('the_content', [$this, 'displayRegistrationForm'], 10, 2);
    }

    public function displayRegistrationForm(string $content): string
    {
        $workshop = $this->repository->findById( get_the_ID() );
        // Only set for post_type = post!
        if (
            get_post_type(get_the_ID()) !== Constants::WORKSHOP_POST_TYPE
            || ! array_key_exists('register', $_GET)
            || ! $workshop->getRegistrationIsOpen()
        ) {
            return $content;
        }

        wp_enqueue_style("wp-block-columns");
        wp_enqueue_style("wp-block-group");
        wp_enqueue_style("wp-block-buttons");


        $html = '<form class="wp-block-group" action="' . admin_url( 'admin-post.php' ) . '" method="POST">';
            $html .= '<div class="wp-block-columns">';
                $html .= '<div class="wp-block-column">';
                    $html .= '<label for="firstname">' . __('Firstname', Constants::TEXT_DOMAIN) . '</label>';
                    $html .= '<input type="text" id="firstname" name="'. Constants::FIRSTNAME_INDEX .'">';
                $html .= '</div>';
                $html .= '<div class="wp-block-column">';
                    $html .= '<label for="lastname">' . __('Lastname', Constants::TEXT_DOMAIN) . '</label>';
                    $html .= '<input type="text" id="lastname" name="'. Constants::LASTNAME_INDEX .'">';
                $html .= '</div>';
            $html .= '</div>';
            $html .= '<label for="email">' . __('E-mail', Constants::TEXT_DOMAIN) . '</label>';
            $html .= '<input type="email" id="email" name="' . Constants::EMAIL_INDEX . '">';
            $html .= '<input type="hidden" name="' . Constants::WORKSHOP_ID_INDEX . '" value="' . $workshop->getId() . '">';
            $html .= '<input type="hidden" name="action" value="' . Constants::REGISTER_FOR_WORKSHOP_ACTION . '">';
            $html .= wp_nonce_field(
                Constants::REGISTER_FOR_WORKSHOP_ACTION,
                Constants::REGISTER_FOR_WORKSHOP_NONCE,
                true,
                false
            );
            $html .= '<div class="wp-block-buttons">';
                $html .= '<div class="wp-block-button">';
                $html .= '<button class="wp-block-button__link" type="submit">' . __('Register', Constants::TEXT_DOMAIN) . '</button>';
                $html .= '</div>';
            $html .= '</div>';
        $html .= '</form>';

        return $html;
    }
}

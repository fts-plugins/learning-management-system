/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import {__} from '@wordpress/i18n';

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-block-editor/#useBlockProps
 */
import {InnerBlocks, useBlockProps, store as blockEditorStore} from '@wordpress/block-editor';
import {useSelect} from "@wordpress/data";

/**
 * Internal dependencies.
 */
import Placeholder from "../../components/placeholder";

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @return {WPElement} Element to render.
 */
export default function Edit(props) {
    const {clientId} = props;
    const blockProps = useBlockProps();
    const hasInnerBlocks = useSelect(
        (select) =>
            select(blockEditorStore).getBlocks(clientId).length > 0,
        [clientId]
    );
    return (
        <div {...blockProps}>
            {!hasInnerBlocks && (
                <Placeholder {...props} allowSkip={false}/>
            )}
            {hasInnerBlocks && (
                <InnerBlocks/>
            )}
        </div>
    );
}

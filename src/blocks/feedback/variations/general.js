import {__} from "@wordpress/i18n";
import blockJson from "../block.json";
import scope from "./scope";
import FeedbackGeneral from "../../../icons/feedback-general";

const general = {
    name: 'feedback-general',
    title: __('General', 'fantassin-learning-management-system'),
    description: __("Message displayed whether the participant's answer is right or wrong.", 'fantassin-learning-management-system'),
    keywords: [
        __('feedback', 'fantassin-learning-management-system'),
        __('general', 'fantassin-learning-management-system'),
    ],
    icon: (<FeedbackGeneral width={48} height={48}/>),
    supports: blockJson.supports,
    innerBlocks: [ [ 'core/paragraph' ] ],
    scope
};

export default general;

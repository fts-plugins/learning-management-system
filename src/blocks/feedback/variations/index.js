import { applyFilters } from '@wordpress/hooks';

import rightAnswer from "./right-answer";
import wrongAnswer from "./wrong-answer"
import general from "./general"

let variations = [
    general,
    rightAnswer,
    wrongAnswer
].map((feedbackType) => ({
    ...feedbackType,
    attributes: {feedbackType: feedbackType.name},
}))

variations[0].isDefault = true;
variations = applyFilters('feedbackVariations', variations );


export default variations;

import {__} from "@wordpress/i18n";
import blockJson from "../block.json";
import scope from "./scope";
import FeedbackRightAnswer from "../../../icons/feedback-right-answer";

const rightAnswer = {
    name: 'feedback-right-answer',
    title: __( 'Right Answer', 'fantassin-learning-management-system' ),
    description: __( 'Message displayed when participant answer is correct.', 'fantassin-learning-management-system' ),
    keywords: [
        __('feedback', 'fantassin-learning-management-system'),
        __('right', 'fantassin-learning-management-system'),
    ],
    icon: (<FeedbackRightAnswer width={48} height={48}/>),
    supports: blockJson.supports,
    innerBlocks: [ [ 'core/paragraph' ] ],
    scope
};

export default rightAnswer;

import {__} from "@wordpress/i18n";
import blockJson from "../block.json";
import scope from "./scope";
import FeedbackWrongAnswer from "../../../icons/feedback-wrong-answer";


const wrongAnswer = {
    name: 'feedback-wrong-answer',
    title: __( 'Wrong Answer', 'fantassin-learning-management-system' ),
    description: __( 'Message displayed when participant answer is incorrect.', 'fantassin-learning-management-system' ),
    keywords: [
        __('feedback', 'fantassin-learning-management-system'),
        __('wrong', 'fantassin-learning-management-system'),
    ],
    icon: (<FeedbackWrongAnswer width={48} height={48}/>),
    supports: blockJson.supports,
    innerBlocks: [ [ 'core/paragraph' ] ],
    scope
};

export default wrongAnswer;

/**
 * WordPress dependencies
 */
import {InnerBlocks, useBlockProps, store as blockEditorStore} from '@wordpress/block-editor';
import {useDispatch, useSelect} from "@wordpress/data";

import OptionAppender from "../../components/option-appender";

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @return {WPElement} Element to render.
 */
export default function Edit({isSelected, clientId}) {
    const blockProps = useBlockProps();

    console.log(clientId);

    let hasChildBlockSelected = useSelect((select) => {
        const {hasSelectedInnerBlock} = select(blockEditorStore)
        return hasSelectedInnerBlock(clientId, true);
    }, []);

    const {updateBlockAttributes} = useDispatch(blockEditorStore);

    /**
     * Get last RadioControl checked to unchecked others.
     */
    useSelect((select) => {
        const {__experimentalGetLastBlockAttributeChanges, getBlock, getBlockRootClientId} = select(blockEditorStore)
        const block = __experimentalGetLastBlockAttributeChanges();

        if (block === null) {
            return;
        }

        const [blockId, attributes] = Object.entries(block)[0];

        if (!attributes.checked) {
            return;
        }

        const rootClientId = getBlockRootClientId(blockId);
        const rootBlock = getBlock(rootClientId);

        if( rootBlock.name !== "fantassin/multi-choice-one-response" ) {
            return;
        }

        rootBlock.innerBlocks?.forEach((block) => {
            if(block.clientId !== blockId) {
                updateBlockAttributes(block.clientId, {checked: false});
            }
        })

    }, []);

    return (
        <div {...blockProps}>
            <InnerBlocks
                template={[['fantassin/option', {type: "radio"}]]}
                allowedBlocks={['fantassin/option']}
                renderAppender={false}
            />
            {(hasChildBlockSelected || isSelected) && (
                <OptionAppender type="radio"/>
            )}
        </div>
    );
}

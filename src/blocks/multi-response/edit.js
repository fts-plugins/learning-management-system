/**
 * WordPress dependencies
 */
import {InnerBlocks, useBlockProps, store as blockEditorStore} from '@wordpress/block-editor';
import {useSelect} from "@wordpress/data";

/**
 * Internal dependencies.
 */
import OptionAppender from "../../components/option-appender";

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @return {WPElement} Element to render.
 */
export default function Edit({isSelected, clientId}) {
    const blockProps = useBlockProps();

    let hasChildBlockSelected = useSelect((select) => {
        const {hasSelectedInnerBlock} = select(blockEditorStore)
        return hasSelectedInnerBlock(clientId, true);
    }, []);

    return (
        <div {...blockProps}>
            <InnerBlocks
                template={[['fantassin/option', {type:"checkbox"}]]}
                allowedBlocks={['fantassin/option']}
                renderAppender={false}
            />
            {(hasChildBlockSelected || isSelected) && (
                <OptionAppender type="checkbox"/>
            )}
        </div>
    );
}

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-block-editor/#useBlockProps
 */
import {InspectorControls, RichText, useBlockProps} from '@wordpress/block-editor';
import {RadioControl, CheckboxControl, Flex, PanelBody, PanelRow, ToggleControl} from "@wordpress/components";
import {__} from "@wordpress/i18n";


/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @return {WPElement} Element to render.
 */
export default function Edit({attributes, setAttributes, clientId}) {
    const blockProps = useBlockProps();
    const {type} = attributes;

    if (!type) {
        setAttributes({type: 'checkbox'});
    }

    const handleChecked = (checked) => {
        setAttributes({checked: Boolean(checked)})
    }


    return (
        <>
            <InspectorControls>
                <PanelBody initialOpen={true}>
                    <PanelRow>
                        <ToggleControl
                            label={__('Is default selected?', 'fantassin-learning-management-system')}
                            checked={attributes.checked}
                            onChange={handleChecked}
                        />
                    </PanelRow>
                </PanelBody>
            </InspectorControls>

            <Flex
                {...blockProps}
                justify="flex-start"
                gap={0}
            >
                {type === 'radio' && (
                    <RadioControl
                        label=""
                        options={[
                            {label: '', value: clientId}
                        ]}
                        selected={attributes.checked && clientId}
                        onChange={handleChecked}
                    />
                )}
                {type === 'checkbox' && (
                    <CheckboxControl
                        label=""
                        checked={attributes.checked}
                        onChange={handleChecked}
                    />
                )}
                <RichText
                    tagName="span"
                    placeholder={__('Example response', 'fantassin-learning-management-system')}
                    value={attributes.content}
                    onChange={(content) => setAttributes({content})}
                />
            </Flex>
        </>
    );
}

import {MenuGroup, Button, Dropdown, MenuItem, NavigableMenu, Toolbar, Flex, FlexItem} from "@wordpress/components";
import {check, replace} from '@wordpress/icons';
import {__} from '@wordpress/i18n';
import {applyFilters} from "@wordpress/hooks";
import questionTypes from "../types";

function QuestionTypeToolbar({questionType, onChange}) {
    const label = __('Question Type', 'fantassin-learning-management-system');

    const options = applyFilters(
        'questionTypesOptions',
        questionTypes.map((qType) => ({
            label: qType.label,
            value: qType.slug,
            description: qType.description
        }))
    );

    const selectedOption = options.find((option) => option.value === questionType) || {label};

    const getMenuItemProps = (option) => {
        let children = (
            <Flex direction="column" align="flex-start" gap={0}>
                <FlexItem isBlock><strong>{option.label}</strong></FlexItem>
                {option?.description && (
                    <FlexItem isBlock><small>{option.description}</small></FlexItem>
                )}
            </Flex>
        );

        const props = {};
        props.children = children;
        if (option.disabled) {
            props.onClick = () => {
            };
        }

        return props;
    };

    return (
        <Toolbar>
            <Dropdown
                popoverProps={{
                    isAlternate: true,
                    position: 'bottom right left',
                    focusOnMount: true,
                }}
                renderToggle={({isOpen, onToggle}) => (
                    <Button
                        onClick={onToggle}
                        icon={selectedOption?.icon || replace}
                        aria-expanded={isOpen}
                        aria-haspopup="true"
                    >
                        {selectedOption?.label}
                    </Button>
                )}
                renderContent={({onClose}) => (
                    <NavigableMenu role="menu" stopNavigationEvents>
                        <MenuGroup label={label}>
                            {options.map((option) => {
                                const isSelected = option.value === selectedOption?.value;
                                const menuItemProps = getMenuItemProps?.(option);
                                return (
                                    <MenuItem
                                        key={option.value}
                                        role="menuitemradio"
                                        isSelected={isSelected}
                                        icon={isSelected ? check : null}
                                        onClick={() => {
                                            onChange(option.value);
                                            onClose();
                                        }}
                                        children={option.label}
                                        {...menuItemProps}
                                    />
                                );
                            })}
                        </MenuGroup>
                    </NavigableMenu>
                )}
            />
        </Toolbar>
    )
}

export default QuestionTypeToolbar;

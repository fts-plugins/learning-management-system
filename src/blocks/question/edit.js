/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import {__} from '@wordpress/i18n';

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-block-editor/#useBlockProps
 */
import {BlockControls, InnerBlocks, useBlockProps, store as blockEditorStore} from '@wordpress/block-editor';
import {getBlockTypes} from '@wordpress/blocks';
import {useSelect} from "@wordpress/data";

/**
 * Internal dependencies.
 */
import QuestionTypeToolbar from "./components/question-type-toolbar";
import Placeholder from "../../components/placeholder";

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @return {WPElement} Element to render.
 */
export default function Edit(props) {
    const {clientId, attributes, setAttributes} = props;
    const blockProps = useBlockProps();
    const {questionType} = attributes;
    const blockTypes = getBlockTypes();
    const allowedBlocks = blockTypes.filter(
        (block) => {
            return block.name !== 'fantassin/assessment-step' && block.name !== 'fantassin/question'
        }
    ).map(block => block.name);

    const TEMPLATE = [
        ['fantassin/label'],
        ['fantassin/input-text'],
        ['fantassin/feedbacks']
    ]

    const hasInnerBlocks = useSelect(
        (select) =>
            select(blockEditorStore).getBlocks(clientId).length > 0,
        [clientId]
    );
    return (
        <div {...blockProps}>
            {!hasInnerBlocks && (
                <Placeholder {...props} allowSkip={false}/>
            )}
            {hasInnerBlocks && (
                <>
                    <BlockControls>
                        <QuestionTypeToolbar
                            questionType={questionType}
                            onChange={(nextQuestionType) => setAttributes({questionType: nextQuestionType})}
                        />
                    </BlockControls>
                    <InnerBlocks
                        template={TEMPLATE}
                        allowedBlocks={allowedBlocks}
                    />
                </>
            )}
        </div>
    );
}

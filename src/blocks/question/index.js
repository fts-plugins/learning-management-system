/**
 * Registers a new block provided a unique name and an object defining its behavior.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-registration/
 */
import { registerBlockType } from '@wordpress/blocks';
import { help } from '@wordpress/icons';

/**
 * Internal dependencies
 */
import variations from './variations';
import edit from './edit';
import save from './save';
import QuestionIcon from '../../icons/question';

/**
 * Every block starts by registering a new block type definition.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-registration/
 */
registerBlockType( 'fantassin/question', {
	icon: QuestionIcon,
	/**
	 * @see ./variations.js
	 */
	variations,
	/**
	 * @see ./edit.js
	 */
	edit,
	/**
	 * @see ./save.js
	 */
	save,
} );

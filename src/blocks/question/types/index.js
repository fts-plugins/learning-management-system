import { applyFilters } from '@wordpress/hooks';

import shortAnswer from "./short-answer";
import multiResponse from "./multi-response"
import multiChoiceOneResponse from "./multi-choice-one-response"
import trueFalse from "./true-false"
import likertScale from "./likert-scale"

const questionTypes = applyFilters(
    'questionTypes',
    [
        shortAnswer,
        multiResponse,
        multiChoiceOneResponse,
        trueFalse,
        likertScale,
    ]
);

export default questionTypes;

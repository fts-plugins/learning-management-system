import {__} from "@wordpress/i18n";
import LikertScaleIcon from "../../../icons/likert-scale";

const likertScale = {
    label: __('Likert Scale', 'fantassin-learning-management-system'),
    slug: 'likert-scale',
    description: __('Code is poetry!', 'fantassin-learning-management-system'),
    icon: <LikertScaleIcon size={48} />,
    keywords: [
        __('likert', 'fantassin-learning-management-system'),
        __('scale', 'fantassin-learning-management-system'),
    ],
};

export default likertScale;

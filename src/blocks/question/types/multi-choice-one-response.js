import {__} from "@wordpress/i18n";
import RadioIcon from "../../../icons/radio";

const multiChoiceOneResponse = {
    label: __('Multi Choice (One Response)', 'fantassin-learning-management-system'),
    slug: 'multi-choice-one-response',
    description: __('Code is poetry!', 'fantassin-learning-management-system'),
    icon: RadioIcon,
    keywords: [
        __('radio', 'fantassin-learning-management-system'),
        __('one-response', 'fantassin-learning-management-system'),
        __('multi-choice', 'fantassin-learning-management-system')
    ],
    innerBlocks: [['fantassin/label'], ['fantassin/multi-choice-one-response']],
};

export default multiChoiceOneResponse;

import {__} from "@wordpress/i18n";
import CheckboxIcon from "../../../icons/checkbox";

const multiResponse = {
    label: __( 'Multi Response', 'fantassin-learning-management-system' ),
    slug: 'multi-response',
    description: __( 'Code is poetry!', 'fantassin-learning-management-system' ),
    icon: CheckboxIcon,
    keywords: [
        __('checkbox', 'fantassin-learning-management-system'),
        __('multi-response', 'fantassin-learning-management-system'),
        __('multi-choice', 'fantassin-learning-management-system')
    ],
    innerBlocks: [['fantassin/label'], ['fantassin/multi-response']],
};

export default multiResponse;

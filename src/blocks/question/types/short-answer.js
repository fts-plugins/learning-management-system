import {__} from "@wordpress/i18n";
import InputTextIcon from "../../../icons/input-text";

const shortAnswer = {
    label: __( 'Short Answer', 'fantassin-learning-management-system' ),
    slug: 'short-answer',
    icon: InputTextIcon,
    description: __( 'Code is poetry!', 'fantassin-learning-management-system' ),
    keywords: [
        __('text', 'fantassin-learning-management-system')
    ],
    innerBlocks: [['fantassin/label'], ['fantassin/input-text']],
};

export default shortAnswer;

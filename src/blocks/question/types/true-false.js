import {__} from "@wordpress/i18n";
import TrueFalseIcon from "../../../icons/true-false";

const trueFalse = {
    label: __('True / False', 'fantassin-learning-management-system'),
    slug: 'true-false',
    description: __('Code is poetry!', 'fantassin-learning-management-system'),
    icon: TrueFalseIcon,
    keywords: [
        __('true', 'fantassin-learning-management-system'),
        __('false', 'fantassin-learning-management-system'),
        __('boolean', 'fantassin-learning-management-system'),
    ],
};

export default trueFalse;

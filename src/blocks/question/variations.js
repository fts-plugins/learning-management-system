import {applyFilters} from "@wordpress/hooks";

/**
 * Internal dependencies.
 */
import questionTypes from './types';

let variations = questionTypes.map((questionType) => ({
    name: questionType.slug,
    title: questionType.label,
    icon: questionType.icon,
    attributes: {questionType: questionType.slug},
    keywords: questionType.keywords,
    innerBlocks: questionType.innerBlocks,
}))

variations[0].isDefault = true;
variations = applyFilters('questionTypesVariations', variations );

export default variations;

/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';
import {store as blockEditorStore, useBlockEditContext} from '@wordpress/block-editor';
import {Button} from '@wordpress/components';
import {plus} from '@wordpress/icons';
import {useDispatch} from "@wordpress/data";
import {createBlock} from '@wordpress/blocks';

function OptionAppender({type}) {

    if (!type) {
        type = "checkbox";
    }

    const {clientId} = useBlockEditContext();
    const {insertBlock, startTyping} = useDispatch(blockEditorStore);

    const label = __('Add an option', 'fantassin-learning-management-system');

    const handleClick = () => {


        const block = createBlock('fantassin/option', {type});
        insertBlock(block, undefined, clientId);
        // insertDefaultBlock(undefined, clientId);
        // startTyping();
    }

    return (
        <Button
            isSmall
            icon={plus}
            onClick={handleClick}
        >
            {label}
        </Button>
    )
}

export default OptionAppender;

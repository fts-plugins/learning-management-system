import {
    __experimentalBlockVariationPicker,
    store as blockEditorStore,
} from '@wordpress/block-editor';
import {useDispatch, useSelect} from '@wordpress/data';
import {
    createBlocksFromInnerBlocksTemplate,
    store as blocksStore,
} from '@wordpress/blocks';
import {help} from '@wordpress/icons';

function Placeholder({clientId, name, setAttributes, allowSkip}) {

    const {blockType, defaultVariation, variations} = useSelect(
        (select) => {
            const {
                getBlockVariations,
                getBlockType,
                getDefaultBlockVariation,
            } = select(blocksStore);

            return {
                blockType: getBlockType(name),
                defaultVariation: getDefaultBlockVariation(name, 'block'),
                variations: getBlockVariations(name, 'block'),
            };
        },
        [name]
    );
    const {replaceInnerBlocks} = useDispatch(blockEditorStore);

    return (
        <__experimentalBlockVariationPicker
            icon={blockType?.icon?.src || help}
            label={blockType?.title}
            variations={variations}
            onSelect={(nextVariation = defaultVariation) => {
                if (nextVariation.attributes) {
                    setAttributes(nextVariation.attributes);
                }
                if (nextVariation.innerBlocks) {
                    replaceInnerBlocks(
                        clientId,
                        createBlocksFromInnerBlocksTemplate(
                            nextVariation.innerBlocks
                        ),
                        true
                    );
                }
            }}
            allowSkip={allowSkip}
        />
    )
}

export default Placeholder;

import { Icon } from '@wordpress/components';

const CheckboxIcon = (props) => (
    <Icon
        icon={ () => (
            <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M7 11.5L10.5 15L17.5 8" stroke="currentColor" strokeWidth="1.5" fill="none"/>
                <path d="M3.75 19V5C3.75 4.30964 4.30964 3.75 5 3.75H19C19.6904 3.75 20.25 4.30964 20.25 5V19C20.25 19.6904 19.6904 20.25 19 20.25H5C4.30964 20.25 3.75 19.6904 3.75 19Z" stroke="currentColor" strokeWidth="1.5" strokeLinejoin="round" fill="none"/>
            </svg>
        ) }
        {...props}
    />
);

export default CheckboxIcon;

import {SVG, Path} from '@wordpress/primitives';

function FeedbackGeneral({height = 24, width = 24}) {
    return (
        <SVG width={width}
             height={height}
             viewBox="0 0 48 48"
             xmlns="http://www.w3.org/2000/svg"
        >
            <Path
                d="M29 14H34C34.5304 14 35.0392 14.2107 35.4143 14.5858C35.7893 14.9609 36 15.4696 36 16V37C36 37.5304 35.7893 38.0392 35.4143 38.4143C35.0392 38.7893 34.5304 39 34 39H14C13.4696 39 12.9609 38.7893 12.5858 38.4143C12.2107 38.0392 12 37.5304 12 37V16C12 15.4696 12.2107 14.9609 12.5858 14.5858C12.9609 14.2107 13.4696 14 14 14H19C19 12.6739 19.5268 11.4021 20.4645 10.4645C21.4021 9.52679 22.6739 9 24 9C25.3261 9 26.5979 9.52679 27.5356 10.4645C28.4732 11.4021 29 12.6739 29 14Z"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <Path
                d="M24 14C23.7239 14 23.5 13.7761 23.5 13.5C23.5 13.2239 23.7239 13 24 13"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
            />
            <Path
                d="M24 14C24.2761 14 24.5 13.7761 24.5 13.5C24.5 13.2239 24.2761 13 24 13"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
            />
            <Path
                d="M17 22H24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <Path
                d="M28 22H31"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <Path
                d="M31 27H24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <Path
                d="M20 27H17"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <Path
                d="M17 32H24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <Path
                d="M28 32H31"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
                strokeLinecap="round"
                strokeLinejoin="round"
            />

        </SVG>
    )
}

export default FeedbackGeneral;

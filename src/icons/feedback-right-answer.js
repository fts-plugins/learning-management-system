import {SVG, Path} from '@wordpress/primitives';

function FeedbackRightAnswer({height = 24, width = 24}) {
    return (
        <SVG width={width}
             height={height}
             viewBox="0 0 48 48"
             xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M15 22.0147H22"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M15 27.0147H19"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M15 32.0147H19"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M21 39H11C10.4696 39 9.96085 38.7893 9.58579 38.4143C9.21071 38.0392 9 37.5304 9 37V16C9 15.4696 9.21071 14.9609 9.58579 14.5858C9.96085 14.2107 10.4696 14 11 14H16C16 12.6739 16.5268 11.4021 17.4645 10.4645C18.4021 9.52679 19.6739 9 21 9C22.3261 9 23.5979 9.52679 24.5356 10.4645C25.4732 11.4021 26 12.6739 26 14H31C31.5304 14 32.0392 14.2107 32.4143 14.5858C32.7893 14.9609 33 15.4696 33 16V19"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M21 14.0147C20.7239 14.0147 20.5 13.7908 20.5 13.5147C20.5 13.2385 20.7239 13.0147 21 13.0147"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
            />
            <path
                d="M21 14.0147C21.2761 14.0147 21.5 13.7908 21.5 13.5147C21.5 13.2385 21.2761 13.0147 21 13.0147"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
            />
            <path
                d="M31 39C35.4183 39 39 35.4183 39 31C39 26.5817 35.4183 23 31 23C26.5817 23 23 26.5817 23 31C23 35.4183 26.5817 39 31 39Z"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M34.5649 28.6733L30.6916 33.8387C30.6055 33.9532 30.4959 34.048 30.3699 34.1164C30.244 34.1849 30.1049 34.2256 29.962 34.2357C29.8191 34.2459 29.6756 34.2252 29.5413 34.1751C29.4071 34.1249 29.2851 34.0467 29.1836 33.9453L27.1836 31.9453"
                fill="none"
                stroke="currentColor"
                strokeWidth="2.5"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </SVG>
    )
}

export default FeedbackRightAnswer;

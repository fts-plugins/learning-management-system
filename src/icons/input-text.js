import { Icon } from '@wordpress/components';

const InputTextIcon = (props) => (
    <Icon
        icon={ () => (
            <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M4 10H20M4 14H14" stroke="currentColor" strokeWidth="1.5"/>
            </svg>
        ) }
        {...props}
    />
);

export default InputTextIcon;

import { Icon } from '@wordpress/components';

const LikertScaleIcon = (props) => (
    <Icon
        icon={ () => (
            <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <circle cx="4.5" cy="12" r="2.75" stroke="currentColor" strokeWidth="1.5" fill="none"/>
                <circle cx="19.5" cy="12" r="2.75" stroke="currentColor" strokeWidth="1.5" fill="none"/>
                <circle cx="12" cy="12" r="2.25" stroke="currentColor" strokeWidth="1.5" fill="none"/>
                <path d="M7.5 12H9.5M14.5 12H16.5" stroke="currentColor" strokeWidth="1.5"/>
            </svg>
        ) }
        {...props}
    />
);

export default LikertScaleIcon;

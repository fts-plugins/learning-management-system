import { Icon } from '@wordpress/components';

const RequiredIcon = (props) => (
    <Icon
        icon={ () => (
            <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12 21V3" stroke="black" strokeWidth="2"/>
                <path d="M4.20549 16.5L19.7939 7.5" stroke="currentColor" strokeWidth="1.5"/>
                <path d="M4.20549 7.5L19.7939 16.5" stroke="currentColor" strokeWidth="1.5"/>
            </svg>

        ) }
        {...props}
    />
);

export default RequiredIcon;

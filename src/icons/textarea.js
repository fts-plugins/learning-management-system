import { Icon } from '@wordpress/components';

const TextareaIcon = (props) => (
    <Icon
        icon={ () => (
            <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M3 7H21M3 12H21M3 17H15" stroke="currentColor" strokeWidth="1.5"/>
            </svg>
        ) }
        {...props}
    />
);

export default TextareaIcon;

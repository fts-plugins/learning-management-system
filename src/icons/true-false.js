import { Icon } from '@wordpress/components';

const TrueFalseIcon = (props) => (
    <Icon
        icon={ () => (
            <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path d="M2 8H11M2 16H11M14 13L20 19M14 19L20 13M14 7L16.5 9.5L21 5" stroke="currentColor" strokeWidth="1.5" fill="none"/>
            </svg>
        ) }
        {...props}
    />
);

export default TrueFalseIcon;
